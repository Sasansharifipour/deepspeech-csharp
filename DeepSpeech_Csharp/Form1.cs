﻿using NAudio.CoreAudioApi;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Speech.AudioFormat;
using System.Speech.Recognition;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DeepSpeech_Csharp
{
    public partial class Form1 : Form
    {
        private WasapiCapture _audioCapture;
        SpeechRecognitionEngine recognizer;
        DeepSpeechClient.DeepSpeech client;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string model_path = ConfigurationSettings.AppSettings["model_path"].ToString();
            

            recognizer = new SpeechRecognitionEngine(new System.Globalization.CultureInfo("en-US"));

            var lst = new List<string>();
            lst.Add("Hi");
            lst.Add("Simut");
            lst.Add("Print");
            lst.Add("Freeze");
            lst.Add("clean");
            lst.Add("save");
            lst.Add("store");
            lst.Add("database");

            Choices choice = new Choices(lst.ToArray());
            GrammarBuilder builder = new GrammarBuilder(choice);
            builder.Culture = new System.Globalization.CultureInfo("en-US");//??en-GB
            Grammar g = new Grammar(builder);
            recognizer.LoadGrammar(g);

            recognizer.SpeechDetected += Recognizer_SpeechDetected;
            recognizer.SpeechRecognized += Recognizer_SpeechRecognized;
            recognizer.SpeechRecognitionRejected += Recognizer_SpeechRecognitionRejected;

            recognizer.SetInputToDefaultAudioDevice();
            recognizer.RecognizeAsync(RecognizeMode.Multiple);

            client = new DeepSpeechClient.DeepSpeech(model_path);
        }

        private void Recognizer_SpeechRecognitionRejected(object sender, SpeechRecognitionRejectedEventArgs e)
        {
            short[] data = getArrayFromRecognizedAudio(e.Result.Audio);

            var stt = client.SpeechToText(data, (uint)data.Length);
            detected_words(stt);
        }

        private void Recognizer_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            short[] data = getArrayFromRecognizedAudio(e.Result.Audio);

            var stt = client.SpeechToText(data, (uint)data.Length);

            detected_words(stt);
        }

        public short[] getArrayFromRecognizedAudio(RecognizedAudio inputAudio)
        {
            SpeechAudioFormatInfo speechAudioFormatInfo = inputAudio.Format;

            MemoryStream audioStream = new MemoryStream();
            inputAudio.WriteToAudioStream(audioStream);
            byte[] byteArray = audioStream.ToArray();

            long numSamplesInAudio = byteArray.Length / speechAudioFormatInfo.BlockAlign * speechAudioFormatInfo.ChannelCount;
            short[] audioArray = new short[numSamplesInAudio];
            for (int i = 0; i < byteArray.Length; i += speechAudioFormatInfo.BlockAlign / speechAudioFormatInfo.ChannelCount)
            {
                if (speechAudioFormatInfo.BitsPerSample == 16)
                {
                    int audioIndex = i / 2;
                    audioArray[audioIndex] = 0;

                    audioArray[audioIndex] |= (short)(byteArray[i + 1] << 8);
                    audioArray[audioIndex] |= (short)byteArray[i];
                }
                else 
                    audioArray[i] = (short)byteArray[i];
            }

            return audioArray;
        }

        private void Recognizer_SpeechDetected(object sender, SpeechDetectedEventArgs e)
        {
            SpeechRecognitionEngine engine = (SpeechRecognitionEngine)sender;

            int x = 0;
        }

        private void detected_words(string word)
        {
            textBox1.AppendText(word);
            textBox1.AppendText(Environment.NewLine);
        }

    }
}
